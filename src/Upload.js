import React, { Component } from 'react'
import { Button, Menu } from 'semantic-ui-react'
import ReactFileReader from 'react-file-reader';
import Quagga from 'quagga';
import axios from 'axios';


class Upload extends Component {
    constructor(props){
        super(props);
        this.state = {
            file : "",
            searched_food: 'Barcode couldnt be scanned, please take a clearer photo or try searching in the search bar instead.'
        }

        this.nutritionixUrl = 'https://trackapi.nutritionix.com/v2/search/item?upc=0072940756040'
        this.nutritionix_key = '&appId=7de62c39&appKey=f5053a8b99f04ff5b555557900180e0f'

        this._handleFile = this._handleFile.bind(this);
        this._readBarcode = this._readBarcode.bind(this);
    }

    myCallBack = () => {
        console.log("got here")
        this.props.callbackFromParent(this.state.searched_food)
    }

    _handleFile = (files) => {
        this.setState({
            file: files.base64
        })
        console.log(files.base64)
        var obj = this
        console.log("do i ever get here")
        Quagga.decodeSingle({
            decoder: {
                readers : [
                    "upc_reader",
                    "upc_e_reader"
                ]
            },
            numOfWorkers: 1,
            locator: {
                patchSize: "large"
            },
            locate: true,
            src: files.base64
        }, async function(result) {
            console.log("result of quagga")
            console.log(result)
            if (result) {
                let nutUrl = 'https://trackapi.nutritionix.com/v2/search/item?upc=' + result.codeResult.code
                await axios.get(nutUrl, {
                    headers: {
                        'x-app-id': '7de62c39',
                        'x-app-key': 'f5053a8b99f04ff5b555557900180e0f'
                    }
                }).then((response) => {
                    obj.setState({
                        searched_food: response.data.foods[0].food_name
                    })   
                }).catch((error) => {
                    console.log(error);
                    obj.setState({
                        searched_food: 'Sorry, no associated food with barcode found. Try searching for the item in the search bar instead.'
                    })
                });
            }
            obj.myCallBack()
        });
        console.log("hello???????????????")
    }

    _readBarcode = (barcode) => {

        Quagga.decodeSingle({
            decoder: {
                readers : ["upc_reader"]
            },
            locate: true,
            src: barcode
        }, function(result) {
            console.log(result)
        });
    }
    render () {
        return (
            <ReactFileReader base64={true} handleFiles={this._handleFile}>
                <Button className='btn'>Upload Barcode Image</Button>
            </ReactFileReader>
        )
    }
    
}

export default Upload;