import firebase_admin
from firebase_admin import firestore
from firebase_admin import credentials
import flask
from flask_cors import CORS

config = {
    
};


app = flask.Flask(__name__)
cors = CORS(app)

cred = credentials.Certificate("/home/james/Downloads/cs498final-firebase-adminsdk-kbc2j-8e1b6f7524.json")
firebase_admin.initialize_app(cred, config)
# firebase = pyrebase.initialize_app(config)
db=firestore.client()
users = db.collection(u'users')
users_in_db = users.get()
for u in users_in_db:
    print(u'{} => {}'.format(u.id, u.to_dict()))
print("HI")


@app.route('/')
def index():
    return 'Root', 200

@app.route('/users', methods=['GET'])
def get_all_users():
    all_users = flask.jsonify(users.get())
    ret_users = []
    for user in all_users:
        ret_users.append(u'{} => {}'.format(user.id, user.to_dict()))
    return json_util.dumps(ret_users), 200

@app.route('/users', methods=['POST'])
def create_user():
    req = flask.request.json
    user = users.push(req)
    return flask.jsonify({'id': user.key}), 201

@app.route('/users/<id>', methods=['GET'])
def get_user(id):
    return flask.jsonify(ensure_user(id))

@app.route('/users/<id>', methods=['PUT'])
def update_user(id):
    ensure_user(id)
    req = flask.request.json
    users.child(id).update(req)
    return flask.jsonify({'success': True})

@app.route('/users/<id>', methods=['DELETE'])
def delete_user(id):
    ensure_user(id)
    users.child(id).delete()
    return flask.jsonify({'success': True})

def ensure_user(id):
    user = users.child(id).get()
    if not user:
        flask.abort(404)
    return user

if __name__ == '__main__':
    app.run(debug=True)
#
# Users:
# 	id
# 	name
# 	email
# 	user_lists
# Lists:
# 	id
# 	username
# 	list name
# 	food items: (name of food (str) , barcode (str))
#
# OR
#
# Food:
# 	name (str)
# 	barcode (str)
