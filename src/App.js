import React, { Component } from 'react';
// import firebase from "firebase";
//
// import firebaseClient from "./database/firebaseClient";
import logo from './logo.svg';
import './App.css';
import Nutrients from './nutrients'
import AppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import SearchFood from './SearchFood';
import Profile from './profile'
import Login from './Login';

import { HashRouter as Router, Redirect, Route, Link, Switch } from "react-router-dom";

import { Grid } from '@material-ui/core';

import firebase from "firebase";
import firebaseClient from "./database/firebaseClient";
import axios from 'axios'

const provider = new firebase.auth.GoogleAuthProvider();


const styles = theme => ({
  root: {
      flexGrow: 1,
      width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

function PrivateRoute ({component: Component, authed, ...rest}) {
  console.log(authed)
  return (
    <Route
      {...rest}
      render={(props) => authed === true
        ? <Component {...props} />
        : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
    />
  )
}

function PublicRoute ({component: Component, authed, ...rest}) {
  return (
    <Route
      {...rest}
      render={(props) => authed === false
        ? <Component {...props} />
        : <Redirect to='/profile' />}
    />
  )
}


class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      authed: false,
      loading: true,
      user: '',
      email: ''
    }
    this.checkUser = this.checkUser.bind(this)
    
  }

  componentDidMount () {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ 
          user: user,
          authed: true,
          loading: false,
        });
      }
      else{
        this.setState({ 
          user: '',
          authed: false,
          loading: false 
        });
      }
    });
  }

  componentWillUnmount () {
    //this.removeListener()
  }

  signIn = async () => {
    try {
        const result = await firebase.auth().signInWithPopup(provider);
        const token = result.credential.accessToken;
        const user = result.user;
        await this.setState({
          authed: true,
          email: user.email,
          user: user.displayName
        })
        this.checkUser()
        console.log(user.email)
      } catch (error) {
      console.log('failed to log in: ' + error.message);
      }
    };

  signOut = () => {
    firebase.auth().signOut();
  };

  checkUser () {
    var query = {"email": this.state.email} 
    console.log(this.state.email)
    axios.get(`https://secret-retreat-62687.herokuapp.com/api/users`, {
      params: {
        where: query
      }
    })
      .then(response => {
        console.log(response.data.data)
        if (response.data.data.length === 0) {
           axios.post('https://secret-retreat-62687.herokuapp.com/api/users', {'email': this.state.email, 'name': this.state.user})
           .then(res => {
              console.log(res)
           }).catch(err => {
             console.log(err)
           })
        }
      })
      .catch(err => console.log(err));
  }

  render () {
 

    
  return (
    <Router>
    <div>
    <AppBar position="static" style={{ backgroundColor: "#72c460" }}>
        <Toolbar>
          <Grid container direction="row" justify="flex-end" alignItems="center">
            <Button class="grow" component={ Link } to="/nutrients">
              Nutrition Search
            </Button>
          
            <IconButton component={ Link } to="/profile">
              <AccountCircle></AccountCircle>
            </IconButton>
          </Grid>
        </Toolbar>
      </AppBar>
      {/*<header className="App-header">
          {this.state.user === '' && <button onClick={this.signIn}>Log in!</button>}
          {this.state.user && <button onClick={this.signOut}>Log out!</button>}
  </header>*/}
      <div>
       
        <Route exact path = "/" component = {Login}></Route>
         <Route exact path= "/editList" component={SearchFood}/>
        <Route exact path= "/nutrients" component={Nutrients}/>
        <Route exact path= "/profile" component={Profile}/>
      </div>
      </div>
    </Router>
  )
  }
  

// const provider = new firebase.auth.GoogleAuthProvider();
// const db = firebaseClient.firestore();
//
// class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       user: null
//     };
//
//     firebase.auth().onAuthStateChanged(user => {
//       if (user) {
//         this.setState({ user});
//       }
//       else{
//         this.setState({ user: null });
//       }
//     });
//   }
//
//   signIn = async () => {
//     try {
//         const result = await firebase.auth().signInWithPopup(provider);
//         const token = result.credential.accessToken;
//         const user = result.user;
//       } catch (error) {
//       console.log('failed to log in: ' + error.message);
//       }
//     };
//
//   signOut = () => {
//     firebase.auth().signOut();
//   };
//
//   render() {
//     const { user } = this.state;
//     return (
//       <div className="App">
//       <h1>hello</h1>
//         <header className="App-header">
//           {!user && <button onClick={this.signIn}>Log in!</button>}
//           {user && <button onClick={this.signOut}>Log out!</button>}
//         </header>
//         <h1>hi</h1>
//       </div>
//     );
//  }
}

export default App;
