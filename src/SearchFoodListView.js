import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, Button } from 'semantic-ui-react'
import _ from 'lodash'

class SearchFoodListView extends Component {
    clickHandler() {

    }

    render() {
        const noFoods = Object.entries(this.props.foods).length === 0 && this.props.foods.constructor === Object;
        console.log(noFoods)
        if(noFoods) {
            return (
                <List>
                    <List.Item>No Foods Searched Yet!</List.Item>
                </List>
            )
        } else {
            const foodListItems = this.props.foods.map((foo, i) => (
                <List.Item>
                    <List.Content floated='right'>
                        <Button onClick={this.clickHandler}>Add</Button>
                    </List.Content>
                    <List.Content floated='left'>
                        {foo.name}
                    </List.Content>
                </List.Item>
            ));

            return (
                <List>{foodListItems}</List>
            )
        }
    }
}

export default SearchFoodListView;