import React, { Component } from "react";
import { Card, Image, Icon, Grid, Modal, Input, Popup} from 'semantic-ui-react';
import axios from 'axios';
import './profile.css';
import { HashRouter as Router, Redirect, Route, Link, Switch } from "react-router-dom";
import Button from '@material-ui/core/Button';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
        createListModalOpen: false,
        input: '',
        new_list_name: '',
        user_lists: []
    }
  }

openModal = () => {
    this.setState({
        createListModalOpen: true
    });
}

getListsFromDb() {
  var query = '{"assignedUser": "5ccbe44df75b2a002438ec4b"}'
  axios.get(`https://secret-retreat-62687.herokuapp.com/api/lists?where=` + query)
    .then(response => {
      console.log(`https://secret-retreat-62687.herokuapp.com/api/lists?where=` + query)
      this.setState({
        user_lists: response.data.data,
      })
        console.log(response.data.data)
    })
    .catch(err => console.log(err));
}

handleDelete(index) {
  var array = [...this.state.user_lists]; // make a separate copy of the array
  array.splice(index, 1);
  this.setState({
    user_lists: array
  });
  axios.delete(`https://secret-retreat-62687.herokuapp.com/api/lists/` + this.state.user_lists[index]._id)
      .then(response => {
        console.log(response)
      })
      .catch(err => console.log(err));
}

displayLists= () =>
  this.state.user_lists.map((el,index) => {
    return (
    <Grid.Column width={3}>
     <Card>
      <Image src='http://clipart-library.com/data_images/142515.jpg' />
      <Card.Content>
        <Card.Header>{el.name}</Card.Header>
        <Card.Meta>
          <span className='date'>{el.foodItems.length} items</span>
        </Card.Meta>
      </Card.Content>
      <Card.Content extra>
      <Grid>
      <Grid.Column width={10} floated='left'>
          <Button component={Link} to={{pathname:'/editList', state: el}}><Icon name='edit'/> Edit List </Button>
      </Grid.Column>
      <Grid.Column width={5}>
          <Popup trigger={<Icon name='trash alternate' value={index} onClick = {() => {this.handleDelete(index)}} />}>
            Delete this list
          </Popup>
      </Grid.Column>
          </Grid>
      </Card.Content>
     </Card>
   </Grid.Column>

  )
  }
);

componentDidMount() {
  this.getListsFromDb()
}

handleListNameChange = (event) => {
  this.setState({
      input: event.target.value
  })
}

createListModalClose = (event) => {
    if (event.target.value === 'created') {
      axios.post(`https://secret-retreat-62687.herokuapp.com/api/lists`, {
        name: this.state.input,
        assignedUser: "5ccbe44df75b2a002438ec4b",
      }).then((response) => { 
        this.getListsFromDb()
      }).catch((error) => { 
        console.log(error)
      })
    }
   this.setState({
      createListModalOpen: false
   })
}
    render() {
        return (
        <div>
            <Grid columns='equal'>
                <Grid.Row centered='true' color='black'>
                    <Icon size='huge' name='user'/>
                    <h1>Shivani Gupta</h1>
                </Grid.Row>
                    <Grid.Column floated='left'>
                    <div className='title'>
                        <h1>Current Food Lists</h1>
                      </div>
                    </Grid.Column>
                    <Grid.Column floated='right'>
                    
                          <Button color='blue' onClick={this.openModal}> New List </Button>
                 
                    </Grid.Column>
              </Grid>
              <div class='lists'>
                <Grid>
                      {this.displayLists()}
                </Grid>
              </div>

            <Modal size='tiny' open={this.state.createListModalOpen} onClose={this.createListModalClose}>
                <Modal.Header>Create a list of pantry items</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                    <Popup
                        trigger={<Input transparent fluid focus placeholder='Name your list...' onChange={this.handleListNameChange}/>}
                        content='Examples include "Parents Home", "College Apartment", "Work" '
                        on='hover'
                        position='bottom left'
                      />
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='black' value='cancel' onClick={this.createListModalClose}>
                        Cancel
                    </Button>
                    <Button
                      positive
                      content="Create"
                      value='created'
                      onClick={this.createListModalClose}
                    />
                </Modal.Actions>
            </Modal>
        </div>
    )
    }
}
export default Profile;