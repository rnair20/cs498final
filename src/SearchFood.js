import React, { Component } from 'react'
import { Button, List, Grid, Segment, Input, Form, Radio, Menu, Card, Modal, Icon, Image} from 'semantic-ui-react'
import "./searchFood.css"
import axios from 'axios';
import Upload from './Upload'
class SearchFood extends Component {
    constructor(props) {
        super(props);

        this.state ={
            query: '',
            valueRank: 'Standard%20Reference',
            searchData:[],
            list_items: [],
            modalOpen: false,
            addModalOpen: false,
            addanyway: false,
            item_to_add: '',
            quantity: '1',
            upload_food: '',
            list_data: this.props.location.state
        };

        this.usdaBaseUrl = 'https://api.nal.usda.gov/ndb/search/?format=json'
        this.usdaApi_key = '&api_key=iccoh3i8iLOisTSVHDKvbMhBN3FlvCgjmsHKSRnY'

        this.nutritionixUrl = 'https://apibeta.nutritionix.com/v2/search'
        this.nutritionix_key = '&appId=7de62c39&appKey=f5053a8b99f04ff5b555557900180e0f'
        this.handleDelete = this.handleDelete.bind(this)
        this.inputChangeHandler = this.inputChangeHandler.bind(this)
        this.handleRadioChange = this.handleRadioChange.bind(this)
        this.handleQuantityChange = this.handleQuantityChange.bind(this)
        this.addClickHandler = this.addClickHandler.bind(this)
        this.addModalClose = this.addModalClose.bind(this)
        this.modalClose = this.modalClose.bind(this)
        this.getFoods = this.getFoods.bind(this)
        this.handleKeyDown = this.handleKeyDown.bind(this)
    }
    componentDidMount(){
       this.setState({
        list_data: this.props.location.state
       })

      
    }
    handleKeyDown(e) {
    if (e.key === 'Enter') {
      this.addModalClose();
      console.log('do validate');
    }
  }
    modalClose(event) {
        if (event.target.value === 'addanyway') {
            this.setState({
                addAnyway: true,
                addModalOpen: true
            });
            // this.addModalClose()
        }
        this.setState({
            modalOpen: false
        })
    }

    handleDelete(index){
      var array = [...this.state.list_items]; // make a separate copy of the array
      array.splice(index, 1);
      this.setState({list_items: array});
    }

    addModalClose() {
        // if (this.state.addAnway) {
        //     console.log(this.state.addAnyway)
        //     this.setState({
        //         addModalOpen: true,
        //         addAnyway: false
        //     })
        // }
        // else {
            var item_info = {}
            item_info['name'] = this.state.item_to_add
            item_info['quantity'] = this.state.quantity
            var item_info2 = []
            item_info2.push(item_info)
            console.log(item_info2)
            this.setState({
                addModalOpen: false,
                addAnyway: false,
                list_items: this.state.list_items.concat(item_info2),
                quantity: 1
            }); 
        // }
        

    }

    myCallBack = (dataFromChild) => {
        this.setState({upload_food: dataFromChild});
        this.displaySearchFoods()
    }

    makeListInDb = () => {
       var food_items = this.state.list_items.map((elem) => {
            var index = elem.name.indexOf("UPC")
            var name = elem.name
            var quantity = elem.quantity
            var barcode = ''
            if(index >=0) {
              var end = index - 2 
              name =  elem.name.slice(0, end)
              console.log(name)
              barcode = elem.name.slice(index + 5, elem.name.length)
             console.log(barcode)
            }
            return {name: name, barcode: barcode, quantity: quantity}            

          })
       axios.put(`https://secret-retreat-62687.herokuapp.com/api/lists/5cccbb80d251bc00240bafda`, {
            name: 'work',
            assignedUser: '5ccbe44df75b2a002438ec4b',
            foodItems: food_items
       })
    }


    displayList() {
        if (this.state.list_items.length === 0) {
            return (<div>
               <div className = "parent">
              <Image  fsize='medium' src = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQEhMREhMQExUVGBYQEhUQFRsVFhUVIBsgIhobGhokIDAlJCYxJR8VJDUhMTAvOjAvGys/Pz84QjA5MCsBCgoKDQ0NGBAQGCslHSU0Nzc3Nzc3Nzc3NzcwNzc3MDc3Nzc3NzcwNzc3NzA3Nzc3NzE3Nzc3MDA3NzcxNzA3Lv/AABEIAJgA0gMBEQACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAwQFBgcCAf/EAEIQAAIBAgMDCQUECQMFAQAAAAECAAMRBBIhBTFBBhMiUWFxgZHBMlJyobEHM0LRFCNDYoKSsuHwNFPCFSRz0vEW/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMFAQIEBgf/xAA2EQACAQMCAwUGBgICAwAAAAAAAQIDBBEFIRIxQRNRYXGxIjKBkcHRBhQ0QqHwIzMV8VKS4f/aAAwDAQACEQMRAD8A3GAEAIAQAgBACAEAIAQAgBACAEA4qVAoLMQAASSeAG8wDHqn2hYtcRUqIwakWJWk6ggJw1Go07ZP2awW6sabgk+ZrWzsQ1WlTqMhpl1VyhNypI3SFlTJYeB1MGAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQCP2pjCmVEtna9idQqjexHHeAB1mcGoXqtKXG1lvkjaEeJkJVxgOhr12N8p5sn2r2t0BYG/CecWp6lVklB7vphfUm4IISdF44jFp8VaovyM3ld6vD3k/8A1+yHDTYrSSrvp4qqw/fIceciWt3cXiXp/wDB2cRVdpYmn7eVh15cw8ctiPIzrpa9U/dFP+H9UYdJBtbaXPYd6bUzaoMham2dMp3m41GnZO163SlTfBlT6J/3BqoSi00VzB8gKJr0qlOoWpAio6t0rgbgGHWeHVed9jfVa6anHl1OuV7PgcXzHXLzEbSWui4WqtOnkuAtrlr65iR3WlrRhGSeSteSE2VtXbS1qal1qhmClamSxHEXABGl9ZJOlBRbGWa1OM2CAEAIAQAgBACAEAIAQAgBACAEAIAQAgEJjzeu3YiDzLE+k8p+Ipe3Tj4Mno8mMNmJcUAABmqc5YfxP+U5tKjxags9M/wsGZ+4Wqe0OcaV9nUn1KAH3l6LeY1kNWhSqrFSKZlNrkQ2OvRcIpatxK2GdV677j3b55jUtMtqT/xSxLu5/wDRNCo3zESmnO0rEnpEDQVB6Ht8+ykzh8E+n8EvkKYfECn+vp+yelUUD2l4m3vD+3daaXqE7aqqVR+y/wCPEjnBNZRG7UxvP1C49mwC/D1/OfQacOGJzDejUKMGG9SGHfN2srBgt+C2xSqAdIK3FW017DxnFKnKJsSM0AQAgBACAEAIAQAgBACAEAIAQDwm0AbYTFirT5xVNjcoDYZgDoR38O+aqWVlGE8rIzfbYpkCvTq0bmwZ7FL/ABKSB42mrqJe8sGrmlz2GeOqfrK7dQW38l/WeU158V3BeC9WddL3TzZiWfDjqUnyS3rGhLN5OXg/VCr7pP1qqopZiFUakk2AE9c3g528Ecu2kZWqKlTm1BYVGXKrHgFvqb9dpFOtGMHN8kaxeeQ32fh2O8jO36yoSL69Xp4SgtaErypKc3/eiJm8CLUWp1GUgAMOcWx0vua3jlP8Rldq1lK3cZN89iSnLIlhBZqq8A9wOxgCfmWlXU3SfgSIgMOLKB1XXyJHpPqdpNzt6c31S9Djaw2KToNRviqhQZh0h+IHq7DMpZDJfY22jTy6lqZ3g717vykFSkny5mUy5KwIuNQdROQydQAgBACAEAIAQAgBACANcRjUTQm591dT/ac1xd0LdZqyS9fkbKLfIhNr8pEoDNUdKI4X6TnuH9jKWWr3Fw+CzpN+L/vqzoo2s6jxFNsfY+qzYJ2GrNSLXHau/wApeZn2Pte9j+cHJVWE8HWyNpUTRpDnKYORRlLAEaWta83hOLitzWMouK3PeUeMp0sO7VFDgjIFO5id3537IqyUYNsVJKMW2Zkm13RCFqNlbo2FjwtbXslT/wAf+aq4ccyRDbu4lLs4dCwbI5VU+i9QEsgZbIN97WOu7cZpa2MtPuZuaeGtvmdVapKEcVVh+p1jOUSYqrSFVSlBTmdb5sx4Zuzs753O4jOST5HE60ZtZ2RY9p1hVenSUgooFZ7biPwD18px6tX2jRXXd+XT++B3wXUksBTsubi2vhw/ztnfZUOyopPmzVvLGW2R+spHsqL/AEn0lb+IY5tovuf0ZJS5kfS+9qfDTP8AUPSeTfuL4/QnKqmOGdqYKs2d1CgnMTmOgE9pZ6vKnSp03RbSSWV5Er0+Tp9rnCfgPcWTS9tWB0FuOu6ekU01lFVOShHLEyRUU21BBHcZun1MpqSyhpsdzZl6iCPX6TafvBGgbExA5imWIFrpqbbjYfKV9RYkzYeYnFJTGZ2VR1nj3dcjckllhySWWRj8paA3Coe0LYfMiQu5prqQO6pLqOdn7Xp1zlXMDbNZhbT6TeFWM+RJTqwn7pIyQkCAEAIAQAgCdZMysoNrgi44TWSymgZdy92pisHUWgjIiumcNTHSOtjqd2sp7bQbeL46zc5eP93PQaXbUa8HOecrp0KBVcsSzEsx3liST4mXcYxguGKwj0EIxgsRWEWnYvLWvSofojkFCMiVD7VNertFtL8JpWUuB8HMoNX0zMJVqHvdV9hri69wMrEdYtvE54WcqU/80Mp+PU8xC0nSl/lhlPx6iuN2pXOGFBmz0wwZW1upsQR3ayWhGFSag9sPkza3cKtRQezT5MZLusN2+XCoRVZ1V1RdxoJVXVXVHQY6jhNnQg6nHJZZl0YSnxtZZ5VqnKBe2W5vu7pwuzp9tUnNezg5FYRnXm+HKfqW3k3tlESmHY6sedIswKW6NiNdPWeX7Ok7njbzHb+Cvlx0p9i4tNeD+BpNDEI6h0ZWXeCDpaX6kmso2TyRW2MUjNRVWQnMx0YE2yNwlRrjTs35okpSTkM6f3z/AAU/6nnjn/rXm/odPUoeycQybSzKuc89UGXQXBLA+QuZ620lw0oPwXoejqxjLSUm8bL1L7iMGtWoalQA3t0PwjS3jOmd7UcOCOyPMqkupCbR2O1ImpSJKjUg6kD1HznZa3+fZqczhq2s6T46PLuIXZ9U5nA0zWseFtSSPCWFzdRjT4otZIXXXBxLmPgRwBbtO7zMoZzlN5k8nDKUpe8zt6rm2a5yiwu17KOGu6YcnLmxKTlzZ5RqhwGGoM1NBWm5U5lJU9amxm0ZyjyZtGco8mWLYm3WZhSq65tFe1jfqYes7aNxxPhlzO6hc8T4ZcyyTqOwIAQAgBACAY/9reb9MTN7PMrk/mbN6TaJ6nQsdhLHPP0K1s/ZVSoMxpsKZ0zt0Rf92/td047i9pU3wqScu7n8+4k1LUIW9PMZriXTnnw8CWwGz8DRscVUd20OUDKnkDcyqu7+9nlUUor5v0OOjqN3qG1tTeO/H15DXKjlyjdAE5OvLfS/hLX/AJOtOlFbJ4WfMoNSuby3n2U4OHmufl0FMPQKg63vvB3TknUcpZfMo51JTlxN5Z6lMEdLeN82hWqU37DaNqderTeYSaPKdAbz4XnXU1GtKKing7KuqV5wUU8Pr4iWPwiuACSNeE5J1qtSHBKTaJ9N1u5sqvGsSXj9H0Impg6wuAGYWy3G63UPynC4tPkfSbbW9Kr041HOMW98PCafj4+J7T2rWSk1AvWVCcwTMVAPG47eqb8UlHh6HfCha16iuIKL8dmWL7O671MSqm5VFdySbncAB85W37xQa78HnPxBp9tSkriCSk9ttl1z89jRqX3r/DTH9R9ZSy9xfH6Hm+pmW07c6zqChzPfU3zBjc9ms93pkEqWG09l6ciys686kXTm8qOPhnoWXk/yt3U8Sexav/t+czcWX7qfyNK9pjeA+2ljOcYksMgNl16J7e2cSXCtzy17cSc3BPZEf+kU238N15hVEV6mhT9JS4FxM8cTPEhWbGRPmRvHRPWvqOMzkzkKNS9wbXU2Ntx03zBgc4WuabrUW113XFx2/wD2b05uDyiSnUcJZReNm45a6B17mB3qeqWcJqSyi1hNTjlDubm4QAgBAPGNheAZm7Co64mrTWs/tdOxIB1st9BbgIv7CVzbqFObg/XwZzUb6pSlJZfC/ErXKrlGWxBCZsiKAFcEWa2unV2Ty9Kyna5pzxnv7z2emaFaahaqrN5bfyXd4N9/Mq9euz2zG9uvvvJT2Vva0bfKpRwn9sfQWw+NYMm7hT0Fri/GSRbyis1ywpXFjVjLnu14NdxZlk58YOLa+XnMg6OswDzIOoeMDI3ru6nQtltrbhD93bmdNrCE60IzWU2uuOvf0IJ8cxYtpcgodL6Tn4m3k+zafpVCyodjSzw5zuaJ9m+yxRoPiH0NXUE8Ka8fE3PdaUmo1XOoqa6ep5TXbpVrpxjyjt8ev2+B7yO5QfpOKxYJ0cipSB91ejp4ZT5xeW/Z0YeHMjv9OdrRoza3kt/Pn6ehEcs70cUUKXWr+sQjfmOjDz+stNJuZqEeHpsR6bpqqQqXFOpiae6fJrHr4jSlhVA1AJ4k+kv5VJSeS5jSilusscU2C2vfKOA3gdk5KlCMnxdTz+pfh+nVzVobS7uj+zJDBvRc2VSdL3bW/wA5BjDw1g8lWtq1BpVYOORy+CQ7hl7VPpuhxT6EDSYguJZCUazWsL9kjcnF4NW+FkhJTcZYimaV6ibt7jh2matPmuZhpvdcxzQq5lB8D2TMGpGYtPnsXjYeAWil1bPnsxbcCOFh4y1pU1COxb0acYRwiUkpKEAIAQAgGe7ewn6Eyhj+rckU2GuW2uVurTj2cJ20anEsdxX16XA852ZGY3Z9HEL0wG91lOvgRM1aNOqsTWSSzv7mznx0JuL/AIfmitY7kk4uaTBx1N0W890qK2kvnTfzPaWP40jhRuqfxj9iLo7MqU3DVkKBSLZtxPfunG7SrTftRZjXfxJQrWrp2zy5eey6/EnkYECxHhND51gRevZsvWd/hM42M42HAFpgwcNU4AiBgb4vEqq2uCT2zZJ8yahQqVpqEFuyP2HshsXiFpAEKSWc2tlQHU+neZwXNTsabnJeR9fudSha2SSqKVTCWzT3xzL/AMusW1HDDDYdGLVBzYFME5KQ0O7dfcPGVul2dW5qupjKj6njbSpbxuIu4mlHnv1KRydw+Jw1elX5mpZT0xa10OjfKXtbS7irTceA9BqmtaZc2rp9quLps+a+BoHK3BLWp06y2ZqZzrqBdGFiR3aHwlFo9Ts7rs5bcW3k/wC7FBa3f5ZuTy4vmV5dn1TwQd7E/QT3Csp9Wjaf4lor3abfyPX2O7CxqBb78i3PmT6SRWS6s5pfiaqn7FNfFt/YXobIVbEO9wLDUADwAmZ2FKccPJTXuo17yKjUxheH1HGDqk5gTe27S08+1wzlDOcFWnu0KV8Mr79D1jf/AHhpPmbNZG9Ko1NgjHTgf8+k03i8dDTdeQ4rhjdRYXBt2nqMlRIg5N4PnKopFig3brm4H5A6zFtT4qjTNremp1GmzSMLQFNFRdygKL75cJYWC4SSWELTJkIAQAgBAKt9odDNhQ/+3UVvA3X/AJCT2zxURy3keKi/AzfDVmW5QlDcggbie0bpYuCZU8UobdCTobX4VF/ip6jxG/yvNHFokVWL8B/RxKP7LK3YDr4jfNckgNg6Z306Z71H5TDjF80ZyxP/AKdR/wBql/IPymvZw7l8hxM6GApf7dP+UflM9nDuXyGWdLhKY3U6f8o/KZ4Y9wyxQUgNygdwEyYDDY9KJZmqBG0ABKgEA6DXUi3Vre/ZPK6vQnXrSjUi3t7GM45fLOeeehaWjjGnlY8RKrtJGZnZ0BY7swOVfwjTqHzvLvTLONnbRpdevmcVxUdSbfQ4O0afAk/CrH0neQCK7UK9EUjUXQLmAUgAk5QSbgXJ3cNJSXOkTnKp2LUVPntyfev7sztp3sIxSlu0cpjXAACDQAXZvQD1l3GElFJ7nDKpFvJw2LqHTMi/Ctz8z6TfgZr2ngI1qpAJeo9hv1yj5WhQRhSlJ4R3hmKsMvcPynmNRoRo3GYPnvjuI3zyTM5yQY7SQ6NwGncTxkdRNxNZLKF8OCQCTe1wLbu+SckbckW/kpgkCGrZM7E9IDpACwsT4Xt2yxt4JQz1LO1glBPqWCdB0hACAEAIAQCK5T4fnMJXXjkZh3rqPmJtCXDJM0qR4oOJj9E6t3hvMS4KCe8YsMMejbqLDyJhmau8/PB6pDKGIG6+vCGsmrTjJpDnCg3sGcdzsPWaOETeNSXeOSzD9pUHe35xwI3VSQAk656h/jPoY4EHOQnmUsUzOSACQWY6eccCMtz4c9BPFZEUsVva3bx7ZlRXcIcUnjIstJeCr5CMI0bYnha+YuLAZWKi3GZNpx4ceJziqpDUlH4mN+4CZRmEU4yb6C1drKx6gT8pg0isySEtn35tL3JsCbzLNquON4E6GtaofdCp6mOhvLakl3htPUInvOAe4amEKO2X3IeYd7Nc3sD1fSeT1KMo3bb5PBC01hvkyYU31EgTySCeJtlsdxIBvuAvxmUZQqB/gmDBauSNMim7G9ma69tgAT/nVLG2TUNyztU1T3J6dB0hACAEAIAQDl1BBB3EWPdAMOekadRkO8XQ96sRLiDzBMoascJrubPKO9x238wJsR1N1F+BxSF6ZHYy/WDebxVT8hzgWvl7QPpDNGsTa8xxih0H+E/SYRJD3kcbOP6pPhEy+ZtV99iVP/UP8C/WOhs/9S8w2v8AdHvUfOEKHvj2YIRnszc563eZZLW5ryQVzevTHUrt6RjYReKUvgdbTa1J+0W84XMxRWZocUlsAOoAfKYNG8tsa7N15xvedvIaTLJa22F3IKnSroPdUt4nQR0C2pPxJfFbMenSo1j7NUZh1gkk28rGeV1SLdXj6G1xScYQl0wLYFwVA6tDOOm8xIYvYcc0z3RFLMRuUXPfJYRcnsiWEHJ7Indg7GLNnqpZR7KuPaPWQeAnXQoNPMkddvbtPimi0qoAsNBwAnYdx1ACAEAIAQAgBAMi5U4bm8ZXH72cdzgMfmTLO2eaaKS9TjVa6PBEqpzE8CB56yc5204Jd2Tmh+MfvH56wbVP2vwFNnHRfKGZqf7WPqo6Ldx+kwZjzQ32Wf1Sd3rMvmSVv9jFdl4bncWya6oDpxtc2ldqtxVoWjnTeJbHVawhPhU1lblh/wCjU30KBxcb2IAPC+s8RHWNUq5/ycOPBfYvHa2dH9uX5v7j1diL7ieJJmHfak+dy/kiPhtFyoo6p7EpqNFpKN+izR3F3LnXn8zbjo9KSIvaG0cHRuCyO261JAT3Ztw851UNOvau/aTS8ZM7aNvXqr2aSS8UVfHY0YhglOnkW4Ju1yQDx4T1un061L2alRy8/wC5ObUtLpWtu6+3F4LC3JEy0PJIRwdDm0C7yN5HXDN6kuKTYcxZmYXLMAo7OoDxmJPEc9xlNyxA0/aWwlr0KdEsV5vLlIF9wtbylFWpKrHDLerRVSPCyv8A/wCcrU2yKucX0e4At+9xE4vykovC5HC7OaeFyLbs7BrRpqg4bzbeeJlhCKisIsYRUYpIdzY2CAEAIAQAgBACAEAzT7SqWTEJUH40W/blYg/IrO6zezRW3sE5xfemVktYgdd7TsKxJtN9x6BBhvIYZcth2k+Zg3lLilnyH5EwbiOEoc2gS97cfGGb1JcUmyT5O0AMUr63Kley1jKjXP0UvNep02cnxqPmXGoN3ePrPErqWx3NQZryp2tXqVXpPmRFJApjQEcCeu89Zp9rRhSjOO7fU9Pp9rQjSVRbyY02VsKvidaaWX320Xw6/CTXF7RobTe/cT3F9RobSeX3E9V5MDC0+daoXe4WwFlF9/bIdO1KVxdKCjiOH5nmNY1GdxbuGEo5QwxNbIpa19wt1km09GjzEI8UsCpNtfGDUW5NKa9XDggdJ1Yge6pLfRZFcPEGdVGC7dJdDW5VlsEAIAQAgBACAEAIAQBpW2jRRsr1aSt7rOoPleCSNGrJZUW15DkG+ogjKV9qGGvRpP1M1P8AmW//ABE6bWWJnJdxbjFpZw0UB2uEbtHzFvWWKeSr7OUJSjJNczq/T71+h/vBpj/H5P6HaPcnsPpeDVxwk+8kJgkEcHX5xcxFtSNOwwzepHhlgfcnax/TVTgELeNjKnXP0MvNep2WcVxJ9dy7VeHeJ4hdSzENo7Qp4dDUqMAOA4k9QHGSUKE60+CCyyWlRnVnwQWWZhtPafP1zXdRa46F/wAA/DfunrqFt2NHs4vfv8T1dva9jQ7OL3338TU8HUDU0ZRlUqGVbWsCNBaeOqRcZtN7o8jJNSaZH8pvuf4l9ZbaF+tXkzjvP9L+BScfqaa9bg+A1nt0VlPbifgd7Qe1NzxtYd50hGKSzNFk5B4X/uF6qVMnxNlHyzTju5eykddmszlI0WcJYhACAEAIAQAgBACAVnl5t44LDEp95UPN0z7ptq3gPnabQjllnpVkrmviXurd/YxCo1yWa7E8W1JPEkzpPb5UVtyRo32Tbae9TC1GJVV52nmPs2NmA7NQbd8hqLqea1y0ioxrJe03h/QrfLjlI+NrOoY8yhKU1B6LW/Gesn6TeEcLxLLTrGNvQy17b5/Yr9GqVNxu0JHA2kibTyjqu7OleU3TqrOV8V5E8/tIeu4+V/SdqeVk+Pypun2lN84v0eD1Pbb+E/X8pk0l7kfiSVM6CYMrkNNl+yw6ncfOZZPW95eSJHk5/rx/4z6yo1z9DLzR1WfQtfKHGmhQeqoBK5bA7rkges8hZ0VWrKm9ky6taKrVowbxkzHHY6piHz1GLsdB1DsUT11GjTow4YLCPWUaFKhDEFhf3mWXk3yTZiKuIGVRqtM72+LqHZKm+1SMU4UXl9/2Km+1NYcKL+P2L0BPOlCRPKb7n+JfWXOhfrV5M5bz/S/gU2pSJqI3BQ3mZ7cq1JKDXVnuJo5wBewDBj2gTCMQlw5L5yDwLKj1mFudyhL8UW+viSfACV1zNSnt0LO0puEN+bLXOc6ggBACAEAIAQAgBAKf9pexKmKwytSBZ6TZ8o3spFmA7dx8JvCWGW+jXcKFZqbwpGMkWOultLHQ37p0Hsks+0+RIUMBiUGYpiaSOMtR1pvbId9+sdnGatxOWpWoT2zFyW+Mrme7X2LUw2UtZ6dQXp1aetNxwseB7IUsm1tdQuE0tpLmuqGWCw71WSnTUu7HKqrqT/nXMvYldSMIcc3hIvuN5GYmiq2UVgoGtI9IG1jdTv8AC8lpXMcYex8s1CjOrc1K0OUm3ggcpDnhpYg6EG/Eb51JprYqpezHhezyP6B6IgR5CeEolA17aszC3UYZLOXFgk+T1D/u1e51Vlt4GVGufopea9Tqspe2o+ZZdvYE4ii1IELmKi5F7dIHd4TyFpW7GqqmM4z6F5b1uxqqpjOBHY/J6hhrFVzP776nw4Dwklzf1q+0nhdxJcXlav7z27uhKsbb7DvnGlk5BvVx9FPaq0h3uPzkkaFWXKLfwZvGnOXJN/AY1q1HGlcNSr085OfQZtANdLj6y30qhXoXHaSg8YZi5s6rpe3FpeQ4p8h1/FXc/AgX63no3dT8CvVlT6tkhguSWGpnMQ1UjUc6bj+UADzEjlWnLmyWFvThukTwEiJz2AEAIAQAgBACAEAIAQBA4SmTmKIW68ov52g37SeMZeBUoOoQaDQ7NpWZci5WuWQgFCevLuv2zJIq0008vK6nOA2Rh8PfmaNKmTvNNACfGG2+ZtVuKtX35t/EfzBCMsfsqhX+9pU37WUZh3HfMqTXI1lGMlhrJFtyNwnBai9i1G9SZIq9RfuIvy1H/wATqnyQwg3q7fFUb0MdvUfUK2pL9p3idkYegA9OlTQg+1bW1jfU6ys1SUnbPL7vUljCKeySIDlJjiuFqVKbFWUrY8R0hKPTY061xGL3W/oWNpQTuIQqLZlEO0cXU/aYhvhLek9P+Xtaf7Yr5HoPy9nT5qK+R6uyMXU/ZV2+K/qZh3VpD90f75D81Zw5Sj8vshzR5IYtv2aL8TAfSQy1a1X7m/gRy1a2XJt/As3IzkzVw2KSq7U7ZXWy3JuR127JilqVKvPs4JlbfajCvS4IxZo06ymCAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAQxWGWquVhcb99tZFWowqwcJrKZmMnF5XMi6mwtQVbS4uri+kqY6JSptulJpP4k1S4lUSzzQ4TZZ94D4V/vJFpFP90myHIqNmLxZz5D0k0dLt10b+Iydrs+n1E97H85NGxt1+xGBWnhkXUKoPXbXzk8acIe6kvgBaSAIAQAgBACAEAIAQAgH/2Q==" />
              </div>
              <div className = "centeredEmpty">
               <p> Search foods on the right side to populate this list </p>
               </div>
               </div>
            )
        }
        else {
            return (
              <div className = "container">
                <List  verticalAlign='middle' size="large">
                    {this.state.list_items.map(( elem,index) => (
                    <List.Item>
                    <List.Content floated='right'>
                    <Button icon >
                      <Icon name='trash alternate'size = "large" value = {index} onClick = {() => {this.handleDelete(index)}} />
                    </Button>
                    </List.Content>
                             <List.Header> {elem.name} </List.Header>
                             <List.Content>
                             Quantity: {elem.quantity}
                             </List.Content>
                    </List.Item>
                    ))}
                </List>
                <Button className = "saveButton" color="green" onClick={this.makeListInDb}>
                    Save Changes
                </Button>
                </div>

            )
        }

    }

    addClickHandler(event) {
        if (this.state.addAnyway === false && this.state.list_items.some(el => el.name === event.target.value)) {
            this.setState({
                modalOpen: true
            })
        }
        else {
            this.setState({
                addModalOpen: true,
                item_to_add: event.target.value
            });
        }
        this.displayList()
    }

    displaySearchFoods() {
        // if(this.state.searchData) {
        //     return (
        //         <List.Item>No Foods Searched Yet!</List.Item>
        //     )
        // } else {
        if (this.state.query === '' || this.state.upload_food !== '') {
            if (!this.state.upload_food.includes('searching') && this.state.upload_food !== '') {
                return (
                    <List.Item>
                        <List.Content floated='right'>
                            <Button value={this.state.upload_food} onClick={this.addClickHandler}>Add</Button>
                        </List.Content>
                        <List.Content floated='left'>
                            {this.state.upload_food}
                        </List.Content>
                    </List.Item>
                )
            }
            else {
                return (
                    <List.Item>
                        <List.Content floated='left'>
                            {this.state.upload_food}
                        </List.Content>
                    </List.Item>
                )
            }
        }
        else {
            return (
                this.state.searchData.map((foo, i) => (
                    <List.Item>
                        <List.Content floated='right'>
                            <Button value={foo.name} onClick={this.addClickHandler}>Add</Button>
                        </List.Content>
                        <List.Content floated='left'>
                            {foo.name}
                        </List.Content>
                    </List.Item>
                    ))
            )
        }
        // }
    }
    handleQuantityChange(event) {
        const item_quantity = event.target.value;
        this.setState({
          quantity: item_quantity
        });

    }

    inputChangeHandler(event){
        this.setState({query: event.target.value});

        let usdaUrl = this.usdaBaseUrl + this.usdaApi_key + '&q=' + event.target.value + '&ds=' + this.state.valueRank;
        let nutritionixUrl = this.nutritionixUrl + '?q=' + this.state.query + this.nutritionix_key
        console.log(usdaUrl)
        //axios call to search by query
        axios.get(usdaUrl).then((response) => {
            if (response.data.errors){
                console.log("food not found")
            }
            else {
                this.setState({
                    searchData: response.data.list.item,
                });
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    getFoods(valueRank = this.state.valueRank) {
      console.log("am i here")
      let usdaUrl = this.usdaBaseUrl + this.usdaApi_key + '&q=' + this.state.query + '&ds=' + valueRank;
      let nutritionixUrl = this.nutritionixUrl + '?q=' + this.state.query + this.nutritionix_key
      console.log(usdaUrl)
      //axios call to search by query
      axios.get(usdaUrl).then((response) => {
          if (response.data.errors){
              console.log("food not found")
          }
          else {
              this.setState({
                  searchData: response.data.list.item,
              });
          }
      }).catch((error) => {
          console.log(error);
      });
    }

    handleRadioChange(e, {value}){
        console.log('value of radio' + value)
        this.setState({valueRank: value})
        console.log(this.state.valueRank)
        this.getFoods(value)
    }

    render() {
        return (
            <div>
                <Grid>
                    <Grid.Column width={4}>
                    <div className = "searchFoods">
                      <h1>My work list</h1>
                            {this.displayList()}
                    </div>

                    </Grid.Column>
                    <Grid.Column width={10}>

                            <div className = "searchFoods">
                                <h1>Search foods to add to your list</h1>
                          </div>
                          <div>
                           <div className ="nutrientChoice">
                            <Input className = "sortBy"
                                onChange={this.inputChangeHandler}
                                placeholder='Search for food...'
                                value={this.state.query}
                            />

                           <div  className = "sortBy">
                            <Upload callbackFromParent={this.myCallBack} ></Upload>
                            </div>
                            </div>
                            <div className="overall">
                              <div className ="brandedContainer">
                                <Radio
                                    label='Processed/Branded Food'
                                    name='radioGroup'
                                    value='Branded%20Food%20Products'
                                    checked={this.state.valueRank === 'Branded%20Food%20Products'}
                                    onChange={this.handleRadioChange}
                                />
                                </div>
                                <Radio
                                    label='Standard Food'
                                    name='radioGroup'
                                    value='Standard%20Reference'
                                    checked={this.state.valueRank === 'Standard%20Reference'}
                                    onChange={this.handleRadioChange}
                                />
                                </div>
                        </div>
                        <List> {this.displaySearchFoods()} </List>
                    </Grid.Column>
                </Grid>
                <Modal dimmer='blurring' open={this.state.modalOpen} onClose={this.modalClose}>
                    <Modal.Header>Duplicate Item</Modal.Header>
                    <Modal.Content >
                    <Modal.Description>
                      <p>This item is already in your list</p>
                    </Modal.Description>
                  </Modal.Content>
                  <Modal.Actions>
                    <Button color='black' value='addanyway' onClick={this.modalClose}>
                        Add anyway
                    </Button>
                    <Button
                      positive
                      content="Skip Duplicate"
                      onClick={this.modalClose}
                    />
                  </Modal.Actions>
                </Modal>
                <Modal dimmer='blurring' size='tiny' open={this.state.addModalOpen} onClose={this.addModalClose}>
                    <Modal.Header>Select quantity</Modal.Header>
                    <Modal.Content >
                    <Modal.Description>
                      <Input focus placeholder='Enter quantity...' onChange={this.handleQuantityChange} onKeyDown={this.handleKeyDown}/>
                    </Modal.Description>
                  </Modal.Content>
                  <Modal.Actions>
                    <Button
                      positive
                      content="Done"
                      onClick={this.addModalClose}
                    />
                  </Modal.Actions>
                </Modal>



            </div>
        )
    }
}

export default SearchFood;