import React, { Component } from 'react';
import firebase from "firebase";
//
import firebaseClient from "./database/firebaseClient";
import axios from 'axios'
import { Redirect } from 'react-router-dom'
const provider = new firebase.auth.GoogleAuthProvider();
const db = firebaseClient.firestore();

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
          authed: false,
          loading: true,
          userID: "",
          user: '',
          email: '',
          finished: false
        }
        this.checkUser = this.checkUser.bind(this)
        
      }
    
      componentDidMount () {
        firebase.auth().onAuthStateChanged(user => {
          if (user) {
            this.setState({ 
              user: user,
              authed: true,
              loading: false,
            });
          }
          else{
            this.setState({ 
              user: '',
              authed: false,
              loading: false 
            });
          }
        });
      }
    
      componentWillUnmount () {
      }
    
      signIn = async () => {
        try {
            const result = await firebase.auth().signInWithPopup(provider);
            const token = result.credential.accessToken;
            const user = result.user;
            await this.setState({
              authed: true,
              email: user.email,
              user: user.displayName
            })
            this.checkUser(user)
            console.log(user.email)
          } catch (error) {
          console.log('failed to log in: ' + error.message);
          }
        };
    
      signOut = () => {
        firebase.auth().signOut();
      };
    
      checkUser (user) {
        var query = {"email": this.state.email} 
        console.log(user)
        axios.get(`https://secret-retreat-62687.herokuapp.com/api/users`, {
          params: {
            where: query
          }
        })
          .then(response => {
            console.log(response.data.data)
            if (response.data.data.length === 0) {
               axios.post('https://secret-retreat-62687.herokuapp.com/api/users', {'email': user.email, 'name': user.displayName})
               .then(res => {
                console.log(res.data.data._id)
                  // localStorage.setItem('userID',res.data.data._id)
                  this.setState({
                    finished: true,
                    userID: res.data.data._id
                  })
                  //   console.log(localStorage.getItem('userID'))
                  // console.log(res)
               }).catch(err => {
                 console.log(err)
               })
            }
            else{
              this.setState({
                    finished: true,
                    userID: response.data.data[0]._id
                  })
            }

          })
          .catch(err => console.log(err));
      }
    

  render() {

    const { user } = this.state.user;

    if (this.state.authed && this.state.finished){
        return <Redirect to={{
            pathname: '/profile',
            state: {
              userID: this.state.userID
            }
            }}
        />
     //return <Redirect to='/detail/' />
    }
    
    return (
        <header className="App-header">
          {!user && <button onClick={this.signIn}>Log in!</button>}
          {user && <button onClick={this.signOut}>Log out!</button>}
        </header>
    );
 }
}

export default Login;

