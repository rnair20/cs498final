import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import config from "../creds";

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
