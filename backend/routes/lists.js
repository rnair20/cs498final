var express = require('express'),
router = express.Router(),
lists = require('../models/listSchema');
users = require('../models/userSchema');
mongoose = require('mongoose');

router.get('/', function (req, res) {
  if (req.query.count) {
    lists.count("("+req.query.count+")").exec().then((count)=>{
    res.status(200).send({message:"Successfully obtained count",data:count});
    }).catch((err)=>{
      res.status(500).send({message:"There was an error getting count from server",data:err});
    });
  }
  else{
    lists.find(eval("("+req.query.where+")")).select(eval("("+req.query.select+")"))
    .skip(eval("("+req.query.skip+")")).limit(eval("("+req.query.limit+")"))
    .sort(eval("("+req.query.sort+")")).exec().then((lists)=>{
    res.status(200).send({message:"Successfully obtained user's lists",data:lists});
      }).catch((err)=>{
        res.status(500).send({message:"Error getting user's lists from server",data:err});
      });
    }
});

router.post('/',async (req, res) => {
  let listPost = new lists();
  listPost.name = req.body.name;
  listPost.assignedUser=req.body.assignedUser;
  if (req.body.foodItems) {
    listPost.foodItems=req.body.foodItems;
  }
  else {
    listPost.foodItems=[];
  }
  await listPost.save().then(()=>{
    res.status(201).send({message:"Successfully added list", data:listPost});
  }).catch((err)=>{
    res.status(500).send({message:"Error posting to server",data:err});
  });
  users.put({_id: req.body.assignedUser}, listPost.id)
});

router.delete('/', function (req, res) {
  if (req.query.count) {
    lists.count("("+req.query.count+")").exec().then((count)=>{
    res.status(200).send({message:"Successfully obtained count",data:count});
    }).catch((err)=>{
      res.status(500).send({message:"There was an error getting count from server",data:err});
    });
  }
  else{//handles all the query words through eval and if they are null it doesn't change the results
    lists.find(eval("("+req.query.where+")")).select(eval("("+req.query.select+")"))
    .skip(eval("("+req.query.skip+")")).limit(eval("("+req.query.limit+")"))
    .sort(eval("("+req.query.sort+")")).exec().then((lists)=>{
    res.status(200).send({message:"Successfully deleted user list",data: lists});
    }).catch((err)=>{
      res.status(500).send({message:"Error deleting user list from server",data: err});
    });
  }
});

router.get('/:id', function (req, res) {
  lists.findById(req.params.id).exec().then((list)=>{
  if (list) {
    res.status(200).send({message:"Successfully obtained list",data:list});
  }
  else{
    res.status(404).send({message:"List not found",data:[]});
  }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting list from server",data:err});
  });
});

router.put('/:id', function (req, res) {
  lists.findById(req.params.id).then((list)=>{
  if (list) {
    list.name=req.body.name;
    list.assignedUser=req.body.assignedUser;
    list.foodItems=req.body.foodItems;
    list.save().then((response)=>{
    res.status(201).send({message:"List updated",data:list});
    }).catch((err)=>{
        res.status(500).send({message:"Server error when updating",data:err});
    });
  }
  else{
    res.status(404).send({message:"List not found",data:[]});
  }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting list from server",data:err});
  });
});

router.delete('/:id', function (req, res) {
  lists.findByIdAndRemove(req.params.id).exec().then((list)=>{
    if (list) {
      let user = list.assignedUser;
      users.findById(user).exec().then((user)=>{
        if (user!="" && user.lists!=[]) {
            for(var i=0; i<user.lists.length; i++){
                if (user.lists[i]==list.id) {
                    user.lists=user.lists.splice(i,1);
                }
            }
            user.save().catch((err)=>{
                res.status(500).send({message:"Error updating user to remove list",data:err});
            });
        }
      }).catch((err)=>{
          res.status(500).send({message:"Could not find assigned user to delete list",data:err});
      });
        res.status(200).send({message:"List successfully deleted",data:user});
    }
    else{
      res.status(404).send({message:"List not found",data:[]});
    }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting list from server",data:err});
  });
});


module.exports = router;
