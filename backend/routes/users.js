var express = require('express'),
    router = express.Router(),
    users = require('../models/userSchema');
    lists = require('../models/listSchema');
mongoose = require('mongoose');

router.get('/', function (req, res) {
  if (req.query.count) {
    users.count("("+req.query.count+")").exec().then((count)=>{
      res.status(200).send({message:"Successfully obtained count",data:count});
    }).catch((err)=>{
      res.status(500).send({message:"There was an error getting count from server",data:err});
    });
  }
  else{//handles all the query words through eval and if they are null it doesn't change the results
    users.find(eval("("+req.query.where+")")).select(eval("("+req.query.select+")"))
    .skip(eval("("+req.query.skip+")")).limit(eval("("+req.query.limit+")"))
    .sort(eval("("+req.query.sort+")")).exec().then((users)=>{
      res.status(200).send({message:"Successfully obtained user list",data:users});
    }).catch((err)=>{
      res.status(500).send({message:"Error getting user list from server",data:err});
    });
  }
});

router.post('/',function (req, res) {
  let userPost = new users();
  userPost.name = req.body.name;
  userPost.email = req.body.email;
  userPost.lists=[];
  if (req.body.lists) {
    userPost.lists = req.body.lists;
  }
  userPost.save().then(()=>{
    res.status(201).send({message:"Successfully added user",data:userPost});
  }).catch((err)=>{
    res.status(500).send({message:"Error posting to server",data:err});
  });
});
router.get('/:id', function (req, res) {
  console.log(req.params.id);
  users.findById(req.params.id).exec().then((user)=>{
    console.log(user);
    if (user) {
      console.log(user);
      res.status(200).send({message:"Successfully obtained user",data:user});
    }
    else{
      res.status(404).send({message:"User Not Found",data:[]});
    }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting user from server",data:err});
  });
});

router.put('/:id', function (req, res) {
  users.findById(req.params.id).exec().then((user)=>{
    if (user) {
      user.name=req.body.name;
      user.email=req.body.email;
      if(req.body.lists){
        user.lists=user.lists.concat(req.body.lists);
      }
      user.save().then((response)=>{
        res.status(201).send({message:"User Updated",data:user});
      }).catch((err)=>{
        res.status(500).send({message:"Server Error when Updating",data:err});
      });
    }
    else{
      res.status(404).send({message:"User Not Found",data:[]});
    }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting user from server",data:err});
  });
});

router.delete('/:id', function (req, res) {
  users.findByIdAndRemove(req.params.id).exec().then((user)=>{
    if (user) {
      let user_lists = user.lists;
      for (l = 0; l<user_lists.length; l++) {
        let list = user_lists[l];
        console.log(list.id)
        lists.findById(list.id).exec().then((list)=>{
          lists.delete({_id: list.id})
        }).catch((err)=>{
          res.status(500).send({message:"Could not find user's list to delete",data:err});
        });
      }
      res.status(200).send({message:"User and their lists successfully deleted", data: user});
    }
    else {
      res.status(404).send({message:"User not found",data:[]});
    }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting user from server",data:err});
  });
});

module.exports = router;
