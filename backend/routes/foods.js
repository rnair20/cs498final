var express = require('express'),
    router = express.Router(),
    foods = require('../models/foodSchema');
    lists = require('../models/listSchema');
mongoose = require('mongoose');

router.get('/', function (req, res) {
  if (req.query.count) {
    foods.count("("+req.query.count+")").exec().then((count)=>{
      res.status(200).send({message:"Successfully obtained count",data:count});
    }).catch((err)=>{
      res.status(500).send({message:"There was an error getting count from server",data:[]});
    });
  }
  else{
    foods.find(eval("("+req.query.where+")")).select(eval("("+req.query.select+")"))
    .skip(eval("("+req.query.skip+")")).limit(eval("("+req.query.limit+")"))
    .sort(eval("("+req.query.sort+")")).exec().then((food)=>{
      res.status(200).send({message:"Successfully obtained foods",data:food});
    }).catch((err)=>{
      res.status(500).send({message:"Error getting food list from server",data:[]});
    });
  }
});

router.post('/',function (req, res) {
  let foodPost = new foods();
  foodPost.name = req.body.name;
  foodPost.assignedList=req.body.assignedList;
  foodPost.barcode=req.body.barcode;
  foodPost.quantity=req.body.quantity;
  foodPost.save().then(()=>{
    res.status(201).send({message:"Successfully added food",data:foodPost});
  }).catch((err)=>{
    res.status(500).send({message:"Error posting to server",data:[]});
  });
});

router.get('/:id', function (req, res) {
  foods.findById(req.params.id).exec().then((food)=>{
    if (food) {
      res.status(200).send({message:"Successfully obtained food",data:food});
    }
    else{
      res.status(404).send({message:"Food Not Found",data:[]});
    }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting food from server",data:[]});
  });
});
router.put('/:id', function (req, res) {
  foods.findById(req.params.id).exec().then((food)=>{
    if (food) {
      food.name=req.body.name;
      food.assignedList=req.body.assignedList;
      food.barcode=req.body.barcode;
      food.quantity=req.body.quantity;
      food.save().then((response)=>{
        res.status(201).send({message:"Food Updated",data:food});
      }).catch((err)=>{
        res.status(500).send({message:"Server Error when Updating",data:[]});
      });
    }
    else{
      res.status(404).send({message:"Food Not Found",data:[]});
    }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting food from server",data:[]});
  });
});
router.delete('/:id', function (req, res) {
  foods.findByIdAndRemove(req.params.id).exec().then((food)=>{
    if (food) {
      let list = food.assignedList;
      lists.findById(list).exec().then((list)=>{
        if (list!="" && list.foodItems!=[]) {
          for(var i=0; i<list.foodItems.length; i++){
            if (list.foodItems[i]==food.id) {
              list.foodItems=list.foodItems.splice(i,1);
            }
          }
          list.save().catch((err)=>{
            res.status(500).send({message:"Error updating list to remove food",data:[]});
          });
        }
      }).catch((err)=>{
        res.status(500).send({message:"Could not find assigned list to delete food",data:[]});
      });
      res.status(200).send({message:"food Successfully Deleted",data:list});
    }
    else{
      res.status(404).send({message:"food Not Found",data:[]});
    }
  }).catch((err)=>{
    res.status(500).send({message:"Error getting food from server",data:err});
  });
});

module.exports = router;
