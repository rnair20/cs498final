// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var UserSchema = new mongoose.Schema({
    name: {type:String, required:true}, // name of user
    email:{type:String, required:true, unique: true}, // email address of user
    lists: {type:[String]}, // list of ids of user's lists
},{versionKey: false });

// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);
