// Load required packages
var mongoose = require('mongoose');

// define food embedded document schema
var FoodSchema = new mongoose.Schema({
  name: {type:String, required:true}, // name of food
  barcode: {type:String, default:""}, // barcode of food for nutrients
  quantity: {type:Number, default:1},
});

// Define our list schema
var ListSchema = new mongoose.Schema({
    name: {type:String, required:true}, // name of list
    assignedUser: {type:String, required:true}, // id of user
    foodItems: [FoodSchema] // list of food schema objects
},{versionKey: false });

// Export the Mongoose model
module.exports = mongoose.model('List', ListSchema);
